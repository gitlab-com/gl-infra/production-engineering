<!--

This issue is for executing and providing feedback for a disaster recovery gameday.

This template is designed to help facilitate the DR testing process in this Handbook page:
`https://handbook.gitlab.com/handbook/engineering/infrastructure/team/ops/dr-practice/`

If you are an ops team member who is creating this issue to assign to another team, complete these steps:
1. Set the due date to the end of the quarter.
/due January 31 
1. Consult the schedule (or other system) to assign the issue to the right person.
1. Offer to be the secondary individual for the gameday to assist and help collect feedback.

-->

# Disaster Recovery Gameday

## Overview

Disaster recovery gamedays should be conducted no differently than a normal change request issue.
Be aware that some gameday actions can interrupt services and appropriate caution should be taken.

It is advised that there be two individuals participating in the gameday process.
One person will be responsble for following the process steps and executing them.
The other person will be responsible for taking notes, logging problems, and approving any merge requests.

Most gameday processes should take between two and four hours to execute including cleanup time and preparation.

## Prepare

1. [ ] [Create a new issue in the Production project](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new) and select the appropriate template for the gameday you are performing. [Gameday Templates Location](https://gitlab.com/gitlab-com/gl-infra/production/-/tree/master/.gitlab/issue_templates?ref_type=heads)
1. [ ] Read over the issue and familiarize yourself with the steps and access required.
1. [ ] Ask any questions about the process in this issue. Feel free to reach out to the Production Engineering::Ops team via slack ([#g_production-engineering_ops](https://gitlab.enterprise.slack.com/archives/C04MH2L07JS)) or by pinging this issue's author.
1. [ ] Complete any steps for preparing for the change, including seeking reviews and approvals and notifying of the pending gameday.

## Execute

1. [ ] Follow the steps of the gameday change issue.
1. [ ] Record any notes or problems in this issue that are a result of the gameday.

## Report

1. [ ] Use this issue to report timing information.
1. [ ] Re-assign this issue to the author.

## Context and Reference

- [Past Gamedays](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/?sort=closed_at_desc&state=all&label_name%5B%5D=gamedays&first_page_size=50)
- [Disaster Recovery Practice Handbook](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/ops/dr-practice/)
- [Disaster Recovery Gameday Schedule](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/disaster-recovery/gameday-schedule.md)

/label ~"gamedays" ~"workflow-infra::triage" ~"team::unknown"
