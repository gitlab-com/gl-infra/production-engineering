<!-- title the issue: Incident Manager Offboarding - Team Member Name -->

This issue is to provide a checklist for removing someone from the incident Manager rotation.

## IM Offboarder Details

- Person being removed - {+ GitLab username +}
- Date when they will drop - {example: 2022-03-17}
- Reason for Offboarding - {example: leaving company, changing roles, no longer eligible}

## IM Offboarding checklist:

- [ ] Create an MR that removes the user from the rotation when we do IM updates at the beginning of the month.
- [ ] Remove the person from Pagerduty
- [ ] Announce the change in the `#im-general` channel and let the other shift members know that their slots may be changing
```
:wave: Please be aware that we have updated the [IM PagerDuty schedule](https://gitlab.pagerduty.com/schedules#PK4YI6X) MR_LINK.
Because the change will not take effect immediately we are looking for someone to override the following shifts:
- Shifts to cover

If you have any questions don't hesitate to reach out.
```

/label ~"Production Engineering::P2" ~"team::Ops" ~"workflow-infra::In Progress" ~"IM-Offboarding" ~"IM"
/assign @jarv
