#!/bin/bash

set -e

WORKSPACE="$HOME/workspace"

if [[ -z $PRIVATE_TOKEN ]]; then
    echo "You must set PRIVATE_TOKEN to use this script."
    exit 1
fi

clone_group() {
    group=$1
    pages=$(curl -I "https://gitlab.com/api/v4/groups/$group/projects?private_token=${PRIVATE_TOKEN}" | perl -ne 'm/(?i)X-Total-Pages: (\d+)/ && print $1')
    echo "Fetching page $i for group $i"
    for i in $(seq 1 "$pages"); do
        for r in $(curl "https://gitlab.com/api/v4/groups/$group/projects?private_token=${PRIVATE_TOKEN}&page=$i" | jq '.[] | .ssh_url_to_repo'); do
            b=$(basename "${r//\"/}")
            dir=${b/\.git/}
            if [[ ! -d $dir ]]; then
                echo Cloning "${r//\"/}"
                #Cloning into 'cloudflare-firewall'...
                #remote: ERROR: You are not allowed to download code from this project.
                set +e
                git clone "${r//\"/}"
                set -e
            else
                echo "Repo dir $dir already exists, not doing anything"
            fi
        done
    done
}

# Add more groups as desired, ex `gitlab-restore gitlab-com gitlab-org`.
# These two are a good starting point without being too much
for group in gitlab-cookbooks gitlab-com%2Fgl-infra gitlab-com%2Fgl-infra%2Fk8s-workloads; do
    dir="$(sed 's|%2F|/|' <<< "$WORKSPACE/$group")"
    mkdir -p "$dir"
    cd "$dir"
    clone_group "$group"
done
